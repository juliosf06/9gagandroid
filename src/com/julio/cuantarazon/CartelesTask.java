package com.julio.cuantarazon;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

public class CartelesTask extends AsyncTask<String, Void, Document> {
	
	private Context mcontext;
	private ListView lv;
	
	public CartelesTask(Context context, ListView lv){
		this.mcontext = context;
		this.lv = lv;
	}
	
	@Override
	protected Document doInBackground(String... url) {
		// TODO Auto-generated method stub
		android.util.Log.w("urltask: ",url[0]);
		String feed = HttpLoaderUtil.getStringFromURL(url[0]);
		
		if (feed.equals("")) {
			return null;
		} else {
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document dom;
				InputSource is = new InputSource(new StringReader(feed));
				dom = builder.parse(is);
				//dom = builder.parse(feed);
				return dom;
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	public String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}
	
	
	public void onPostExecute(Document doc) {
		if (doc == null) {
			return;
		} else {
			Element root = doc.getDocumentElement();
			android.util.Log.w("child", root.getElementsByTagName("item").getLength() + "");
			//numero de elementos
			int cantidad = root.getElementsByTagName("item").getLength();
			//creamos colleccion de carteles
			CartelItem[] collection = new CartelItem[cantidad];
			//obtenemos todos los tags item
			NodeList nl = root.getElementsByTagName("item");
			//Recorremos el nodelist
			for (int i = 0; i < cantidad; i++) {
				CartelItem cartel = new CartelItem();
				Node node = nl.item(i);
				Element el = (Element) node;
				//titulo
				NodeList node_title = el.getElementsByTagName("title");
				Element el_title = (Element) node_title.item(0);
				//url web cartel
				NodeList node_link = el.getElementsByTagName("guid");
				Element el_link = (Element) node_link.item(0);
				//url imagen sola
				NodeList node_imagen = el.getElementsByTagName("content:encoded");
				Element el_img = (Element) node_imagen.item(0);
				String html_imagen = getCharacterDataFromElement(el_img);
				String url_imagen=html_imagen.substring(html_imagen.indexOf("src=")+5, html_imagen.indexOf(".jpg")+4);
				
				//Set de valores obtenidos
				cartel.setTitle(getCharacterDataFromElement(el_title));
				cartel.setUrlwebcartel(getCharacterDataFromElement(el_link));
				cartel.setUrlimagen(url_imagen);
				collection[i]=cartel;
			}
			
			ListView lv = this.lv;
			ListViewAdapter lvadaptador = new ListViewAdapter(this.mcontext, collection);
			lv.setAdapter(lvadaptador);
		}
	}
	

}
