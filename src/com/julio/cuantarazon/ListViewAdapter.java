package com.julio.cuantarazon;

import java.io.InputStream;
import java.net.URL;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListViewAdapter extends BaseAdapter{
	
	private Context mcontext;
	private CartelItem[] mitems;
	protected LayoutInflater mLayoutInflater;
	
	public ListViewAdapter(Context context, CartelItem[] collection){
		this.mcontext = context;
		this.mitems = collection;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mitems.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return this.mitems[arg0];
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	//arg0 posicion, arg1 vista de cada item, arg2 listview
	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View v = null;
		if (arg1 == null) { 
			if ( mLayoutInflater == null){
				mLayoutInflater = (LayoutInflater) this.mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			v = mLayoutInflater.inflate(R.layout.item_lista, null);
			TextView text_view_1 = (TextView) v.findViewById(R.id.textView1);
			ImageView image_view_1 = (ImageView) v.findViewById(R.id.imageView1);
			final CartelItem rss_items = this.mitems[arg0];
			InputStream is;
			try {
				is = (InputStream) new URL(rss_items.getUrlimagen()).getContent();
				BitmapDrawable bitmapDrawable = new BitmapDrawable(is);
				image_view_1.setImageDrawable(bitmapDrawable);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e("log_tag", "Error in http connection "+e.toString());
			}

			text_view_1.setText(rss_items.getTitle());
			v.setOnClickListener(new View.OnClickListener() {


				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(Intent.ACTION_VIEW,Uri.parse(rss_items.getUrlimagen()));
					ListViewAdapter.this.mcontext.startActivity(i);
				}
			});
			
		} else{
			v = (View) arg1;
		}
		
		return v;
	}
	
	

}
