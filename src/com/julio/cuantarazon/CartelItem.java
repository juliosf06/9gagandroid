package com.julio.cuantarazon;

public class CartelItem {

	String title="";
	String urlwebcartel="";
	String urlimagen="";
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrlwebcartel() {
		return urlwebcartel;
	}
	public void setUrlwebcartel(String urlwebcartel) {
		this.urlwebcartel = urlwebcartel;
	}
	public String getUrlimagen() {
		return urlimagen;
	}
	public void setUrlimagen(String urlimagen) {
		this.urlimagen = urlimagen;
	}
	
}
