package com.julio.cuantarazon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpLoaderUtil {
	
	static public String getStringFromURL(String endpoint) {
		// Retorna el contenido de una url en un String
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(endpoint);
		StringBuilder sb = new StringBuilder("");
		HttpResponse response;
		InputStream is;
		try {
			response = httpclient.execute(httpget);
			if (response.getStatusLine().getStatusCode() == 200) {
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is));
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
			}
		} catch (IOException e) {

			e.printStackTrace();
		}
		return sb.toString();
	}
}
